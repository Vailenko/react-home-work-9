import React from 'react';
import PropTypes from 'prop-types';

const SigninForm = ({title, apiUrl}) => {
return(
<form>
    <h2>{title}</h2>
    <table align="center">
        <tbody>
        <tr>
            <td  align="right">
                <label>Name: </label>
            </td>
            <td>
                <input/>
            </td>
        </tr>
        <tr>    
            <td align="right">
                <label>Password:</label>
            </td>
            <td>
                <input type = "password" />    
            </td>
        </tr>
        </tbody>
    </table>
    <br></br> 
    <button  onClick = {OnClickEventHandler(apiUrl)}>Login</button>       
</form>
)
} 

SigninForm.propTypes = {
    title: PropTypes.string,
    apiUrl: PropTypes.string
}

SigninForm.defaultProps = {
    title: 'Default title',
    apiUrl: 'https://jsonplaceholder.typicode.com/todos'
}
const OnClickEventHandler = (apiUrl) => {
    fetch(apiUrl)
   .then(response => response.json())
   .then(json => console.log(json));
}

export default SigninForm;