import './App.css';
import SigninForm from './forms/AppSigninForm'

function App() {
  return (
    <div className="App">
    <h1>React home work 1</h1>
    
    <SigninForm title="Login to our application" apiUrl = "https://jsonplaceholder.typicode.com/posts" />

</div>
  );
}

export default App;
